# DirectSlave-install
Install free DirectSlave version 3.4.3 for DirectAdmin control panel on Almalinux, Rocky Linux, CentOS, Debian and Ubuntu.

I have taken it and modified it to work with DirectSlave GO Advanced version for DirectAdmin. This shell script was installed on Almalinux 8/9, ubuntu 20/222 and Rocky linux 8 on 64bit machine all works with NO issues.

# Aims
Running DirectSlave as secondary DNS Cluster for DirectAdmin control panel
<br>Maintain updated documentation / tutorials on how to install & configure DirectSlave GO Advanced

# Installing
```
wget https://gitlab.com/w0uter/directslave-install/-/raw/main/directslave-install.sh
chmod +x directslave-install.sh
./directslave-install.sh (IP server DirectAdmin)
```
## After installation finished, change named.conf config to following
On the server DirectAdmin
<br>options {
<br>	listen-on port 53 { any; };
<br>    listen-on-v6 port 53 { none; };

allow-query     { any; };
<br>              allow-notify    { DirectSlave_IP_server_1, DirectSlave_IP_server_2; };
<br>              allow-transfer  { DirectSlave_IP_server_1, DirectSlave_IP_server_2; };

# Pushing domains form DirectAdmin to Directslave #
To push all domains form the DirectAdmin server to the DirectSlave server, you need the follow command.
``` echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue ```
<br>The command will update serial number of existing zones (no IP will be changed), with it all zones will be transferred to DirectSlave server if it's connected with DA's Multiserver function. (can take some minutes before zones will be transferred)

# What's New? 
- Installing DirectSlave 3.4.3
<br>- Support for Ubuntu, Rocky Linux, CentOS, Debian
<br>- Dynamically create system DirectSlave and systemuser (sudo).
<br>- Check if root is running the script
<br>- Dynamically add values for cookie_auth_key, uid and gid in directslave.conf
<br>- Add some colours

## References 
Original script by [Afrizal](https://github.com/afrizal-id/)
<br>[DirectSlave](https://directslave.com/download)
