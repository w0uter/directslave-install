#/usr/bin/bash
# ------------------------------------------------------------------------------
# @author jordavin,Afrizal
# @updated by Wouter
# @date 05.12.2022
# @version 1.0.1
# @source
# ------------------------------------------------------------------------------
RED='\033[0;31m'    # Red
GREEN='\033[0;32m'  # Green
YELLOW='\033[0;33m' # Yellow
BLUE='\033[0;34m'   # Blue
NC='\033[0m'        # No Color

if [ -z "$1" ]; then
    printf "Usage ${RED}<master ip>${NC}\n"
    exit 0
fi

if [ "$EUID" -ne 0 ]; then
    printf "${RED}Please run this script as root${NC}\n"
    exit 0
fi
masterip=$1

printf "${RED}Saving most outputs to /root/install.log${NC}\n"

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$ID
    VER=$VERSION_ID
elif [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

create_sysuser() {
    sudouser=$1
    if !(whiptail --title "More users" --yesno "We are disable root login for ssh. Is there a user available to login other than root ?\n If not, you may not be able to log in anymore." 8 78); then
        if (whiptail --title "New user" --yesno "Do you want to create a new user with sudo rights ?" 8 78); then
            if [ $(id -u) -eq 0 ]; then
                CONTUSER="y"
                username=$(whiptail --inputbox "Enter a username" 8 39 --title "Username" 3>&1 1>&2 2>&3)
                egrep "^$username" /etc/passwd >/dev/null
                if [ $? -eq 0 ]; then
                    whiptail --title "User exist" --msgbox "The user you trying to create already exist." 8 78
              echo "$username exists already! Exiting...." >>/root/install.log
                    exit
                else

                    while [[ "$passphrase" != "$passphrase_repeat" || ${#passphrase} -lt 6 ]]; do
                        passphrase=$(whiptail --passwordbox "${passphrase_invalid_message}Please enter the passphrase (6 chars min.):" 10 78 3>&1 1>&2 2>&3)
                        if [ $? = 1 ]; then
                            break
                        fi

                        passphrase_repeat=$(whiptail --passwordbox "Please repeat the passphrase:" 10 78 3>&1 1>&2 2>&3)
                        if [ $? = 1 ]; then
                            break
                        fi

                        passphrase_invalid_message="Passphrase too short, or not matching! "

                        if [[ "$passphrase" = "$passphrase_repeat" && ${#passphrase} -ge 6 ]] ## check for match
                        then
                            pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
                            useradd -m -p "$pass" "$username"
                            [ $? -eq 0 ] && usermod -G $sudouser $username | printf "\n${GREEN}User ${username} has been added to system as a sudo user!${NC}\n" || echo "Failed to add a user!"
                        fi
                    done
                fi
            else
                echo "Only root may add a user to the system." >>/root/install.log
                whiptail --title "User rights" --msgbox "Only root may add a user to the system.." 8 78
                exit 2
            fi
        fi
    fi
}

getsshport() {
    sshnow="$(cat /etc/ssh/sshd_config | grep "Port " | sed 's/^.....//')"
}

set_customssh() {
    getsshport
    if (whiptail --title "SSH port" --yesno "You are using now port ${sshnow} for SSH.\n Do you want to change your sshd port ?" 8 78); then
    CONTSSH="y"
    customsshport=$(whiptail --inputbox "Enter the new SSH port" 8 39 --title "SSH port" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        sed -i '/PermitRootLogin/ c\PermitRootLogin no' /etc/ssh/sshd_config
        sed -i -e "s/#Port/Port/g" /etc/ssh/sshd_config >>/root/install.log
        sed -i -e "s/Port $sshnow/Port ${customsshport}/g" /etc/ssh/sshd_config >>/root/install.log
        sed -i -e 's/#UseDNS yes/UseDNS no/g' /etc/ssh/sshd_config >>/root/install.log
        sed -i -e 's/#UseDNS no/UseDNS no/g' /etc/ssh/sshd_config >>/root/install.log
        printf "${GREEN}Your ssh port is ${sshport}${NC}\n" >/root/install.log
        systemctl restart sshd >>/root/install.log
    else
        break
    fi
fi
}

add_dsuser() {
    dsusername=$(whiptail --inputbox "Enter a username" 8 39 --title "Create DirectSlave user" 3>&1 1>&2 2>&3)
        while [[ "$dspassphrase" != "$dspassphrase_repeat" || ${#dspassphrase} -lt 6 ]]; do
            dspassphrase=$(whiptail --passwordbox "${dspassphrase_invalid_message}Please enter the passphrase (6 chars min.):" 10 78 3>&1 1>&2 2>&3)
            if [ $? = 1 ]; then
                break
            fi
            dspassphrase_repeat=$(whiptail --passwordbox "Please repeat the passphrase:" 10 78 3>&1 1>&2 2>&3)
            if [ $? = 1 ]; then
                break
            fi
            dspassphrase_invalid_message="Passphrase too short, or not matching! "
            if [[ "$dspassphrase" = "$dspassphrase_repeat" && ${#dspassphrase} -ge 6 ]] ## check for match
            then
                touch /usr/local/directslave/etc/passwd
                chown $1:$1 /usr/local/directslave/etc/passwd
                /usr/local/directslave/bin/directslave --password "$dsusername":"$dspassphrase"
                /usr/local/directslave/bin/directslave --check >>/root/install.log
                rm -f /usr/local/directslave/run/directslave.pid
            fi
        done
}

service_status() {
    servicename=$1
    STATUS="$(systemctl is-active $servicename)"
    if [ "${STATUS}" = "active" ]; then
        running_service="true" #active
    else
        running_service="false" #inactive
    fi
}

rpm_isInstalled() {
    if rpm -q "$1" >/dev/null ; then
        rpm_status="true"
        return 0 #installed
    else
        rpm_status="false"
        return 1 #not installed
    fi
}

ds_install_config() { 
    # use
    # <named user> <workdir> <bind directory> <location and name named.conf file>
    #"bind" "/var/cache/bind" "/var/lib/bind" "/etc/bind/named.conf.options" (for ubuntu)
    echo "installing and configuring directslave"
    sysuser=$1
    workdir=$2
    named_loc=$3
    conf_file=$4

    cd ~
    wget -q https://directslave.com/download/directslave-3.4.3-advanced-all.tar.gz #>>/root/install.log
    tar -xf directslave-3.4.3-advanced-all.tar.gz
    mv directslave /usr/local/
    cd /usr/local/directslave/bin
    mv directslave-linux-amd64 directslave
    cd /usr/local/directslave/
    chown $sysuser:$sysuser -R /usr/local/directslave

    curip="$(hostname -I | awk '{print $1}')"
    cat >/usr/local/directslave/etc/directslave.conf <<EOF
background      1
host            $curip
port            2222
ssl             off
cookie_sess_id  DS_SESSID
cookie_auth_key $(openssl rand -base64 20)
debug           0
uid             $(id -u $sysuser)
gid             $(id -g $sysuser)
pid             /usr/local/directslave/run/directslave.pid
access_log	/usr/local/directslave/log/access.log
error_log	/usr/local/directslave/log/error.log
action_log	/usr/local/directslave/log/action.log
named_workdir   $workdir
named_conf      $named_loc/namedb/directslave.inc
retry_time	1200
rndc_path	/usr/sbin/rndc
named_format    text
authfile        /usr/local/directslave/etc/passwd
EOF

    echo "Creating directorys and files."
    mkdir $named_loc/namedb
    mkdir $named_loc/data
    touch $named_loc/namedb/directslave.inc
    chown $sysuser:$sysuser -R $named_loc/namedb
    chown $sysuser:$sysuser -R $named_loc/data
    mkdir /var/log/named
    touch /var/log/named/security.log
    touch /var/log/named/general
    touch /var/log/named/notify
    touch /var/log/named/transfers
    touch /var/log/named/dnssec
    touch /var/log/named/query
    chmod a+w -R /var/log/named

    mv $conf_file $conf_file.orig

    cat >$conf_file <<EOF
options {
        directory "$workdir";

        // If there is a firewall between you and nameservers you want
        // to talk to, you may need to fix the firewall to allow multiple
        // ports to talk.  See http://www.kb.cert.org/vuls/id/800113

        // If your ISP provided one or more IP addresses for stable
        // nameservers, you probably want to use them as forwarders.
        // Uncomment the following block, and insert the addresses replacing
        // the all-0's placeholder.

        // forwarders {
        //      0.0.0.0;
        // };

        //========================================================================
        // If BIND logs error messages about the root key being expired,
        // you will need to update your keys.  See https://www.isc.org/bind-keys
        //========================================================================
        dnssec-validation auto;

        listen-on port 53 { any; };
        listen-on-v6 port 53 { none; };
        dump-file       "$named_loc/data/cache_dump.db";
        statistics-file "$named_loc/data/named_stats.txt";
        memstatistics-file "$named_loc/data/named_mem_stats.txt";
        recursing-file  "$named_loc/data/named.recursing";
        secroots-file   "$named_loc/data/named.secroots";
                allow-query     { any; };
                allow-notify    { $masterip; };
                allow-transfer  { none; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion no;
        /* Path to ISC DLV key */
        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};
logging {
        channel default_debug {
                file "$named_loc/data/named.run";
                severity dynamic;
        };
        channel transfers {
            file "/var/log/named/transfers" versions 3 size 5M;
            print-time yes;
            severity info;
        };
      channel notify {
            file "/var/log/named/notify" versions 3 size 5M;
            print-time yes;
            severity info;
        };
        channel dnssec {
            file "/var/log/named/dnssec" versions 3 size 5M;
            print-time yes;
            severity info;
        };
        channel query {
            file "/var/log/named/query" versions 5 size 5M;
            print-time yes;
            severity info;
        };
        channel general {
            file "/var/log/named/general" versions 3 size 5M;
        print-time yes;
        severity info;
        };
    channel slog {
        syslog security;
        severity info;
    };
        category xfer-out { transfers; slog; };
        category xfer-in { transfers; slog; };
        category notify { notify; };

        category lame-servers { general; };
        category config { general; };
        category default { general; };
        category security { general; slog; };
        category dnssec { dnssec; };

        // category queries { query; };
};

include "$named_loc/namedb/directslave.inc";
EOF
    if [[ $OS = "ubuntu" || $OS = "debian" ]] ; then
        echo 'include "/etc/bind/bind.keys";' >> $conf_file
        echo 'include "/etc/bind/zones.rfc1918";' >> $conf_file

    else
        echo 'include "/etc/named.rfc1912.zones";' >> $conf_file
        echo 'include "/etc/named.root.key";' >> $conf_file
    fi
}

ds_systemd() {
    echo "Setting up systemd script"
    cat >/etc/systemd/system/directslave.service <<EOL
[Unit]
Description=Directslave service
DefaultDependencies=no
After=network.target

[Service]
Type=simple
User=$sysuser
Group=$sysuser
RestartSec=5
ExecStart=/usr/local/directslave/bin/directslave --run
ExecStop=/bin/kill -s QUIT \$MAINPID
TimeoutStartSec=0
RemainAfterExit=yes

[Install]
WantedBy=default.target
EOL
    if [[ $OS = "Ubuntu" || $OS = "debian" ]]
    then
        ln -s /etc/systemd/system/directslave.service /lib/systemd/system/directslave.service
    fi
    
    printf "%s\n"
    printf "${GREEN}Setting DirectSlave enabled at boot time and starting up${NC}\n\n"
    chown root:root /etc/systemd/system/directslave.service
    chmod 755 /etc/systemd/system/directslave.service
    systemctl daemon-reload >>/root/install.log
    systemctl enable named >>/root/install.log
    systemctl enable directslave >>/root/install.log
    systemctl restart named >>/root/install.log
    systemctl restart directslave >>/root/install.log
}

ufw_setup() {
REQUIRED_PKG="ufw"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
    echo Checking for $REQUIRED_PKG: $PKG_OK
    if [ "" = "$PKG_OK" ]; then
        if (whiptail --title "Uncomplicated Firewall" --yesno "Uncomplicated Firewall (ufw) is not installed. Do you want to install ufw and opening Firewall ports ?" 8 78); then
            echo "${GREEN}Installing Uncomplicated Firewall${NC}\n"
            apt-get install ufw -y 2>/dev/null >>/root/install.log
            ufw allow 53/tcp
            ufw allow 53/udp
            ufw allow 953/tcp
            ufw allow 2222/tcp
            if [ $customsshport ]; then
                ufw allow $customsshport/tcp
            else
                getsshport
                ufw allow $sshnow/tcp
            fi
            ufw reload
            ufw --force enable
           systemctl start ufw
        fi
    else
        service_status ufw
        if $running_service
        then #service ufw service is active so add the firewall ports
            prinf "Uncomplicated Firewall is ${GREEN}running${NC}, opening Firewalld ports\n"
            ufw allow 53/tcp
            ufw allow 53/udp
            ufw allow 953/tcp
            ufw allow 2222/tcp
            if [ $customsshport ]; then
                ufw allow $customsshport/tcp
            else
                getsshport
                ufw allow $sshnow/tcp
            fi
            ufw reload
        else #service is not running
            if (whiptail --title "Inactive ufw" --yesno "Uncomplicated Firewall (ufw) is installed but not running. Do you want to enable ufw and opening Firewall ports ?" 8 78); then
                ufw allow 53/tcp
                ufw allow 53/udp
                ufw allow 953/tcp
                ufw allow 2222/tcp
                if [ $customsshport ]; then
                    ufw allow $customsshport/tcp
                else
                    getsshport
                    ufw allow $sshnow/tcp
                fi
                ufw reload
                ufw --force enable
                systemctl start ufw
            fi
        fi
    fi
}

firewalld_setup() {
REQUIRED_RPM="firewalld"
rpm_isInstalled $REQUIRED_RPM
echo "Checking for $REQUIRED_RPM"
    if $rpm_status; then
        service_status firewalld
        if $running_service ; then
            #firewalld service is active so add the firewall ports
            printf "Simple firewalld is ${GREEN}running${NC}, opening Firewalld ports 53 tpc/udp, 443 tcp, 953 tcp, 2222 tcp\n"
            firewall-cmd --permanent --add-port=2222/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=53/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=53/udp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=443/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=953/tcp >/root/install.log 2>&1
            if [ $customsshport ]; then
                printf "Opening ssh port $customsshport\n"
                firewall-cmd --permanent --add-port=$customsshport/tcp >/root/install.log 2>&1
            else
                getsshport
                printf "Opening ssh port $sshnow\n"
                firewall-cmd --permanent --add-port=$sshnow/tcp >/root/install.log 2>&1
            fi
            firewall-cmd --reload >/root/install.log 2>&1
        else #service is not running
            if (whiptail --title "Inactive ufw" --yesno "Firewalld is installed but not running. Do you want to enable firewalld and opening Firewall ports ?" 8 78); then
                printf "Opening Firewalld ports 53 tpc/udp, 443 tcp, 953 tcp, 2222 tcp, and ${GREEN}activate firewalld${NC}\n"
                firewall-cmd --permanent --add-port=2222/tcp >/root/install.log 2>&1
                firewall-cmd --permanent --add-port=53/tcp >/root/install.log 2>&1
                firewall-cmd --permanent --add-port=53/udp >/root/install.log 2>&1
                firewall-cmd --permanent --add-port=443/tcp >/root/install.log 2>&1
                firewall-cmd --permanent --add-port=953/tcp >/root/install.log 2>&1

                if [ $customsshport ]; then
                    printf "Opening ssh port $customsshport\n"
                    firewall-cmd --permanent --add-port=$customsshport/tcp >/root/install.log 2>&1
                else
                    getsshport
                    printf "Opening ssh port $sshnow\n"
                    firewall-cmd --permanent --add-port=$sshnow/tcp >/root/install.log 2>&1
                fi
                systemctl start firewalld >/root/install.log 2>&1
                systemctl enable firewalld >/root/install.log 2>&1
            fi
        fi
    else
        if (whiptail --title "Firewalld installation" --yesno "Firewalld is not installed. Do we need to install firewalld and opening Firewalld ports ?" 8 78); then
            echo "Installing firewalld and openening ports 53 tpc/udp, 443 tcp, 953 tcp, 2222 tcp" >>/root/install.log
            dnf install firewalld -y
            firewall-cmd --permanent --add-port=2222/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=53/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=53/udp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=443/tcp >/root/install.log 2>&1
            firewall-cmd --permanent --add-port=953/tcp >/root/install.log 2>&1

            if [ $customsshport ]; then
                printf "Opening ssh port $customsshport\n"
                firewall-cmd --permanent --add-port=$customsshport/tcp >/root/install.log 2>&1
            else
                getsshport
                printf "Opening ssh port $sshnow\n"
                firewall-cmd --permanent --add-port=$sshnow/tcp >/root/install.log 2>&1
            fi
            systemctl start firewalld >/root/install.log 2>&1
            systemctl enable firewalld >/root/install.log 2>&1
        fi
    fi
}

# Start installation procedure 
if [[ ($OS = "AlmaLinux") && (${VER%.*} -ge 7 && ${VER%.*} -le 9) || ($OS = "rocky") && (${VER%.*} -ge 7 && ${VER%.*} -le 9) || ($OS = "centos") && (${VER%.*} -ge 7 && ${VER%.*} -le 8) ]]; then
    printf "System runs on ${OS} version ${VER}.\n"
    printf "${GREEN}OS supported continue....${NC}\n"
    printf "%s\n"
    printf "Doing updates and installs \n\n"
    dnf update -y 
    dnf install epel-release -y 
    dnf install bind bind-utils tar perl wget -y 

    systemctl start named >>/root/install.log
    systemctl stop named >>/root/install.log

    create_sysuser "wheel"
    set_customssh
    ds_install_config "named" "/var/named" "/var/named" "/etc/named.conf"
    add_dsuser "named"
    ds_systemd
    firewalld_setup
    #end centos like install
elif
    [[ ($OS = "ubuntu") && (${VER%.*} -ge 17 && ${VER%.*} -le 22) || ($OS = "debian") && (${VER%.*} -ge 9 && ${VER%.*} -le 11) ]]
    then
    printf "System runs on ${OS} version ${VER}.\n"
    printf "${GREEN}OS supported continue....${NC}\n"
    printf "%s\n"
    printf "Doing updates and installs \n\n"
    apt update 2>/dev/null #>>/root/install.log
    apt install bind9 bind9-utils inetutils-syslogd golang-go perl whiptail -y #2>/dev/null >>/root/install.log
    systemctl start named >>/root/install.log
    systemctl stop named >>/root/install.log
    
    mkdir /var/log/bind
    chown bind:root /var/log/bind
    sed -i '/\#include <local\/usr.sbin.named>/a /var/log/bind/ rw,\
/var/log/bind/** rw,' /etc/apparmor.d/usr.sbin.named

    create_sysuser "sudo"
    set_customssh
    ds_install_config "bind" "/var/cache/bind" "/var/lib/bind" "/etc/bind/named.conf.options"
    add_dsuser "bind"
    ds_systemd
    ufw_setup
    # end debian like installl
else

    echo "Installation failed. System runs on unsupported Linux."
    echo "Only support for version Almallinux, Rocky Linux, CentOS, Debian and Ubuntu."
    echo "you are running: $OS version $VER"
    echo "Exiting..."
    exit 0
fi

echo "Checking DirectSlave and starting"
/usr/local/directslave/bin/directslave --check >>/root/install.log
/usr/local/directslave/bin/directslave --run

printf "${GREEN}all done!${NC}\n"
printf "Open the DirectSlave Dashboard using a web browser ${GREEN}http://${curip}:2222${NC} and login with ${RED}${dsusername}${NC} / ${RED}${dspassword}${NC} \n\n"
printf "if failed browse using IP address, edit /usr/local/directslave/etc/directslave.conf and change the host 127.0.0.1 to your current IP address\n\n"

if [ "$CONTFIRE" = "y" ]; then
    printf "We have installed a Firewall and opening ports 53 (named), 953 (RNDC), 22222 (DirectSlave) and ${customsshport} (SSH)\n"
fi

if [ "$CONTSSH" = "y" ]; then
    printf "SSH port is changed to: ${RED}${customsshport}${NC}\n"
fi
if [ "$CONTUSER" = "y" ]; then
    printf "We have added sudo user ${YELLOW}${username}${NC} with pass ${YELLOW}${password}${NC} to the system.\n\n"
fi
rm /root/directslave-3.4.3-advanced-all.tar.gz -f
exit 0
